package ru.t1.dsinetsky.tm;

import ru.t1.dsinetsky.tm.api.ICommandRepository;
import ru.t1.dsinetsky.tm.constant.ArgumentConst;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.model.Command;
import ru.t1.dsinetsky.tm.repository.CommandRepository;
import ru.t1.dsinetsky.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    public final static ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    private static void terminalRun(final String command) {
        if (command == null) {
            displayError();
            return;
        }
        switch (command) {
            case (TerminalConst.CMD_HELP):
                displayHelp();
                break;
            case (TerminalConst.CMD_VERSION):
                displayVersion();
                break;
            case (TerminalConst.CMD_ABOUT):
                displayAbout();
                break;
            case (TerminalConst.CMD_INFO):
                displayInfo();
                break;
            case (TerminalConst.CMD_ARG):
                displayArguments();
                break;
            case (TerminalConst.CMD_CMD):
                displayCommands();
                break;
            case (TerminalConst.CMD_EXIT):
                exitApp();
                break;
            default:
                displayError();
                break;
        }
    }

    private static void argumentRun(final String arg) {
        switch (arg) {
            case (ArgumentConst.CMD_HELP):
                displayHelp();
                break;
            case (ArgumentConst.CMD_VERSION):
                displayVersion();
                break;
            case (ArgumentConst.CMD_ABOUT):
                displayAbout();
                break;
            case (ArgumentConst.CMD_INFO):
                displayInfo();
                break;
            case (ArgumentConst.CMD_CMD):
                displayCommands();
                break;
            case (ArgumentConst.CMD_ARG):
                displayArguments();
                break;
            default:
                displayError();
                break;
        }
    }

    private static boolean argRun(final String[] args) {
        if (args.length < 1) {
            return false;
        }
        String param = args[0];
        argumentRun(param);
        return true;
    }

    private static void displayHelp() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) {
            System.out.println(command);
        }
    }

    private static void displayVersion() {
        System.out.println("Version: 1.7.0");
    }

    private static void displayAbout() {
        System.out.println("Developer: Sinetsky Dmitry");
        System.out.println("Dev Email: dsinetsky@t1-consulting.ru");
    }

    private static void displayWelcome() {
        System.out.println("Welcome to the task-manager_08!");
        System.out.println("Type \"help\" for list of commands");
    }

    private static void displayError() {
        System.out.println("Invalid input! Type \"help\" or use argument \"-h\" for list of inputs");
    }

    private static void displayInfo() {
        /* Processors, count and print */
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        /* Memory as bytes */
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long usedMemory = totalMemory - freeMemory;
        /* Memory as String */
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryFormat);
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        /* Print memory */
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Used Memory: " + usedMemoryFormat);
        System.out.println("Free memory: " + freeMemoryFormat);

    }

    private static void displayArguments() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) {
            if (command.getArgument() != null && !command.getArgument().isEmpty())
                System.out.println(command.getArgument());
        }
    }

    private static void displayCommands() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) {
            if (command.getName() != null && !command.getName().isEmpty())
                System.out.println(command.getName());
        }
    }

    private static void exitApp() {
        System.exit(0);
    }

    public static void main(final String[] args) {
        if (argRun(args)) {
            exitApp();
            return;
        }
        displayWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("\nEnter command:");
            String command = scanner.nextLine();
            terminalRun(command);
        }

    }

}

